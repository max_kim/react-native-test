import _ from 'lodash';

const hex2rgba = _.memoize(
  (hex: string, a: number = 1, output: 'string' | 'map' = 'string') => {
    const [r, g, b] = hex.match(/\w\w/g)?.map((x) => parseInt(x, 16));

    return output === 'string' ? `rgba(${r},${g},${b},${a})` : { r, g, b, a };
  },
);

export default hex2rgba;
