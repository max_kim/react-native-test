import { Alert } from 'react-native';

export const simpleAlert = ({
  title = 'Attention',
  message,
  onPressOk,
  cancelable = false,
} = {}) => {
  Alert.alert(title, message, [{ text: 'Ok', onPress: onPressOk }], {
    cancelable,
  });
};

export const errorAlert = ({
  title = 'Error',
  message = 'Something went wrong =(',
  onPressOk,
  cancelable = false,
} = {}) => {
  simpleAlert({ title, message, onPressOk, cancelable });
};

export const confirmAlert = ({
  title = 'Confirmation',
  message,
  cancelText = 'Cancel',
  okText = 'Ok',
  onPressOk,
  onPressCancel,
  cancelable = false,
}) => {
  const alertButtons = [
    { text: cancelText, onPress: onPressCancel },
    { text: okText, onPress: onPressOk },
  ];

  Alert.alert(title, message, alertButtons, { cancelable });
};
