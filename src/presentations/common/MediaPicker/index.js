import React, { useCallback, useMemo, forwardRef } from 'react';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';

const MediaPicker = forwardRef(
  ({ title, config, onSuccess, onError, onCancel }, ref) => {
    const pickerConfig = useMemo(
      () => ({
        cropping: false,
        multiple: false,
        includeBase64: false,
        includeExif: true,
        avoidEmptySpaceAroundImage: true,
        useFrontCamera: false,
        mediaType: 'any',
        showsSelectedCount: true,
        forceJpg: true,
        sortOrder: 'none',
        writeTempFile: true,
        ...config,
      }),
      [config],
    );

    const pickerActions = useMemo(
      () => ({
        camera:
          pickerConfig.mediaType === 'video' ? 'Take video' : 'Take photo',
        library: 'Choose from Library',
        cancel: 'Cancel',
      }),
      [pickerConfig.mediaType],
    );

    const actionsValues = useMemo(() => Object.values(pickerActions), [
      pickerActions,
    ]);

    const onSelectAction = useCallback(
      (index) => {
        let pickerAction;
        switch (actionsValues[index]) {
          case pickerActions.camera:
            pickerAction = ImagePicker.openCamera;
            break;
          case pickerActions.library:
            pickerAction = ImagePicker.openPicker;
            break;
          case pickerActions.cancel:
            return onCancel && onCancel();
          default:
            return;
        }

        pickerAction(pickerConfig)
          .then(onSuccess)
          .catch((err) => {
            if (err?.message?.includes('cancelled')) {
              return onCancel && onCancel();
            }

            onError && onError(err?.message);
          });
      },
      [
        actionsValues,
        onCancel,
        onError,
        onSuccess,
        pickerActions,
        pickerConfig,
      ],
    );

    return (
      <ActionSheet
        ref={ref}
        title={title}
        options={actionsValues}
        cancelButtonIndex={2}
        onPress={onSelectAction}
      />
    );
  },
);

export default MediaPicker;
