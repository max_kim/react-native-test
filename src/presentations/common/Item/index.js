import React from 'react';
import styled from 'styled-components';

import { palette } from '../../../core/styleGuide';

const Item = ({ title, children }) => {
  return (
    <ItemContainer>
      <Title>{title}</Title>
      {children}
    </ItemContainer>
  );
};

const ItemContainer = styled.View`
  justify-content: space-between;
  height: 72px;
  width: 100%;
  margin-top: 16px;
`;

const Title = styled.Text`
  font-size: 12px;
  color: ${palette.dustyGray};
`;

export default React.memo(Item);
