import React from 'react';
import styled from 'styled-components';

import { palette } from '../../../core/styleGuide';
import Item from '../Item';

const TextItem = ({ title, text }) => {
  return (
    <Item title={title}>
      <Text>{text}</Text>
    </Item>
  );
};

const Text = styled.Text`
  flex: 1;
  margin-top: 8px;
  font-size: 16px;
  color: ${palette.tundora};
`;

export default React.memo(TextItem);
