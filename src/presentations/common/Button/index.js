import React from 'react';
import { ActivityIndicator } from 'react-native';
import styled from 'styled-components';

import { palette } from '../../../core/styleGuide';

const Button = ({ text, onPress, preloader }) => {
  return (
    <ButtonContainer>
      <Touchable onPress={onPress}>
        <Text>{text}</Text>
        {preloader && (
          <ActivityIndicator
            style={{ marginLeft: 8 }}
            size={'small'}
            color={palette.white}
          />
        )}
      </Touchable>
    </ButtonContainer>
  );
};

Button.defaultProps = {
  isPasswordInput: false,
  preloader: false,
};

const ButtonContainer = styled.View`
  align-items: center;
  justify-content: center;
  width: 100%;
  padding-vertical: 24px;
`;

const Touchable = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  border: 1px solid ${palette.mercury};
  width: 50%;
  background-color: ${palette.mercury}
  padding-vertical: 12px;
  border-radius: 12px;
`;

const Text = styled.Text`
  font-size: 16px;
  color: ${palette.tundora};
`;

export default React.memo(Button);
