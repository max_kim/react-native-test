import React, { useState } from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';
import styled from 'styled-components';
import FastImageLib from 'react-native-fast-image';

import { palette } from '../../../core/styleGuide';

type Props = {
  containerStyle?: any,
  preloaderStyle?: { size?: 'small' | 'large', color?: string },
  showPreloader: boolean,
  style?: any,
  uri: string,
  source?: { [key: string]: any },
  onLoadStart?: (any) => void,
  onLoadEnd?: (any) => void,
  onError?: (any) => void,
  [key: string]: any,
};

const FastImage = (props: Props) => {
  const {
    containerStyle,
    showPreloader,
    preloaderStyle,
    style,
    uri,
    source,
    onLoadStart,
    onLoadEnd,
    onError,
    ...others
  } = props;

  const [preloader, setPreloader] = useState(showPreloader);

  return (
    <Container style={containerStyle}>
      <FastImageLib
        style={{ flex: 1, ...style }}
        source={{ uri, source }}
        onLoadStart={() => {
          setPreloader(true);
        }}
        onLoadEnd={() => {
          setPreloader(false);
        }}
        onError={() => {
          setPreloader(false);
        }}
        {...others}
      />
      {preloader && (
        <SpinnerBox style={StyleSheet.absoluteFill}>
          <ActivityIndicator size={'large'} color={palette.dustyGray} />
        </SpinnerBox>
      )}
    </Container>
  );
};

FastImage.defaultProps = { showPreloader: false };

const Container = styled.View`
  flex: 1;
`;

const SpinnerBox = styled.View`
  justify-content: center;
  align-items: center;
`;

export default FastImage;
