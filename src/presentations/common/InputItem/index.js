import React from 'react';
import styled from 'styled-components';

import { palette } from '../../../core/styleGuide';
import Item from '../Item';

const InputItem = ({ title, text, onChangeText, isPasswordInput }) => {
  return (
    <Item title={title}>
      <Input
        value={text}
        onChangeText={onChangeText}
        secureTextEntry={isPasswordInput}
      />
    </Item>
  );
};

InputItem.defaultProps = {
  isPasswordInput: false,
};

const Input = styled.TextInput`
  flex: 1;
  margin-top: 8px;
  font-size: 16px;
  color: ${palette.tundora};
  border: solid 1px ${palette.mercury};
  border-radius: 4px;
`;

export default React.memo(InputItem);
