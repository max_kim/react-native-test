import React from 'react';
import SafeAreaView from 'react-native-safe-area-view';
import styled from 'styled-components';

import { palette } from '../../../core/styleGuide';

const Container = ({ style, children }) => {
  return <WrappedSafeAreaView style={style}>{children}</WrappedSafeAreaView>;
};

const WrappedSafeAreaView = styled(SafeAreaView)`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding-horizontal: 16px;
  background-color: ${palette.alabaster};
`;

export default Container;
