import React, { useState, useCallback, useEffect } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import Container from '../../../presentations/common/Container';
import InputItem from '../../../presentations/common/InputItem';
import Button from '../../../presentations/common/Button';
import { getErrorMessage } from '../../../store/selectors/auth';
import { palette } from '../../../core/styleGuide';
import biometry from '../../../services/biometry';
import { confirmAlert } from '../../../core/utils';

const Sign = ({ title, buttonText, onPress, errorMessage }) => {
  const [email, setEmail] = useState('eve.holt@reqres.in');
  const [password, setPassword] = useState('cityslicka');
  const [isFetching, setIsFetching] = useState(false);

  const onSuccess = useCallback(async () => {
    setIsFetching(false);
    try {
      const supported = await biometry.isSupported();
      if (supported) {
        confirmAlert({
          message:
            'Would you like to activate Face ID / Touch ID authorization?',
          onPressOk: () => biometry.set(email, password),
        });
      }
    } catch (e) {}
  }, [email, password]);

  const onFinish = useCallback(() => setIsFetching(false), []);

  const onPressButton = useCallback(() => {
    setIsFetching(true);
    onPress(email, password, onSuccess, onFinish);
  }, [onPress, email, password, onSuccess, onFinish]);

  useEffect(() => {
    biometry
      .isSupported()
      .then(() => biometry.isEnabled())
      .then(() => biometry.get())
      .then(({ username: email, password }) =>
        onPress(email, password, onFinish, onFinish),
      );
  }, [onFinish, onPress]);

  return (
    <Container>
      <TitleText>{title}</TitleText>
      <InputItem title={'email'} text={email} onChangeText={setEmail} />
      <InputItem
        title={'password'}
        text={password}
        onChangeText={setPassword}
        isPasswordInput
      />
      <Button
        text={buttonText}
        onPress={onPressButton}
        preloader={isFetching}
      />
      {errorMessage && <ErrorText>{errorMessage}</ErrorText>}
    </Container>
  );
};

const TitleText = styled.Text`
  margin-bottom: 32px;
  font-size: 24px;
  color: ${palette.tundora};
`;

const ErrorText = styled.Text`
  margin-top: 24px;
  font-size: 12px;
  color: ${palette.wildWatermelon};
`;

const mapStateToProps = (state) => ({
  errorMessage: getErrorMessage(state),
});

export default connect(mapStateToProps)(Sign);
