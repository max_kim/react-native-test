import React, { useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';
import styled from 'styled-components';

import { palette } from '../../../core/styleGuide';

const HeaderButton = ({
  onPress,
  label,
  routePath,
  color,
  style,
  textStyle,
}) => {
  const { navigate } = useNavigation();

  const navigateTo = useCallback(() => {
    routePath && navigate(routePath);
  }, [navigate, routePath]);

  return (
    <ButtonWrapper onPress={onPress || navigateTo} style={style}>
      <ButtonText color={color} style={textStyle}>
        {label}
      </ButtonText>
    </ButtonWrapper>
  );
};

HeaderButton.defaultProps = {
  label: 'go next',
};

const ButtonWrapper = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  padding-horizontal: 8px;
`;

const ButtonText = styled.Text`
  font-size: 16px;
  color: ${palette.tundora};
`;

export default React.memo(HeaderButton);
