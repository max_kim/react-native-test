import React, { useState } from 'react';
import { StyleSheet, ActivityIndicator } from 'react-native';
import styled from 'styled-components';
import FastImage from 'react-native-fast-image';

import { palette } from '../../../../core/styleGuide';
import { hex2rgba } from '../../../../core/utils';

const Avatar = ({ uri, size, isUploading }) => {
  const [inProgress, setInProgress] = useState(false);

  return (
    <AvatarWrapper size={size}>
      <AvatarImage
        size={size}
        onLoadStart={() => setInProgress(true)}
        onLoadEnd={() => setInProgress(false)}
        onError={() => setInProgress(false)}
        source={{
          uri: uri,
          priority: FastImage.priority.high,
        }}
        fallback={true}
      />
      {(isUploading || inProgress) && (
        <IndicatorWrapper style={StyleSheet.absoluteFill}>
          <ActivityIndicator
            size={'large'}
            color={hex2rgba(palette.white, 0.8)}
          />
        </IndicatorWrapper>
      )}
    </AvatarWrapper>
  );
};

Avatar.defaultProps = {
  isUploading: false,
};

const AvatarWrapper = styled.View`
  align-items: center;
  width: ${({ size }) => size}px;
  height: ${({ size }) => size}px;
  border-radius: ${({ size }) => size / 2}px;
  background-color: ${palette.mercury};
  overflow: hidden;
`;

const AvatarImage = styled(FastImage)`
  width: ${({ size }) => size}px;
  height: ${({ size }) => size}px;
`;

const IndicatorWrapper = styled.View`
  justify-content: center;
`;

export default Avatar;
