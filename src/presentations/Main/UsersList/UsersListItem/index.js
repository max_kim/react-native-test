import React from 'react';
import styled from 'styled-components';

import FastImage from '../../../common/FastImageWithPreloader';
import { palette } from '../../../../core/styleGuide';

const UsersListItem = ({
  item: { id, avatar, first_name, last_name },
  onPress,
}) => {
  return (
    <ListItemBlock onPress={() => onPress(id)}>
      <ImageBox>
        <FastImage uri={avatar} clone />
      </ImageBox>

      <ItemText>{`${first_name} ${last_name}`}</ItemText>
    </ListItemBlock>
  );
};

const ListItemBlock = styled.TouchableOpacity`
  width: 100%;
  padding-top: 8px;
  padding-bottom: 18px;
  flex-direction: row;
  align-items: center;
`;

const ImageBox = styled.View.attrs({
  overflow: 'hidden',
})`
  width: 56px;
  height: 56px;
  border-radius: 4px;
`;

const ItemText = styled.Text`
  flex: 1;
  margin-left: 16px;
  font-size: 22px;
  color: ${palette.tundora};
`;

export default React.memo(UsersListItem);
