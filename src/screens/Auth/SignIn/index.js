import React from 'react';
import { connect } from 'react-redux';

import Sign from '../../../presentations/Auth/Sign';
import { authActions } from '../../../store/sagas/auth/actions';

const SignIn = ({ login, errorMessage }) => {
  return (
    <Sign
      title={'Sign In'}
      buttonText={'login'}
      onPress={login}
      errorMessage={errorMessage}
    />
  );
};

const mapDispatchToProps = (dispatch) => ({
  login: (email, password, onSuccess, onError) =>
    dispatch(authActions.login({ email, password, onSuccess, onError })),
});

export default connect(null, mapDispatchToProps)(SignIn);
