import React from 'react';
import { connect } from 'react-redux';

import Sign from '../../../presentations/Auth/Sign';
import { authActions } from '../../../store/sagas/auth/actions';

const SignUp = ({ register, errorMessage }) => {
  return (
    <Sign
      title={'Sign Up'}
      buttonText={'register'}
      onPress={register}
      errorMessage={errorMessage}
    />
  );
};

const mapDispatchToProps = (dispatch) => ({
  register: (email, password, onSuccess, onError) =>
    dispatch(authActions.register({ email, password, onSuccess, onError })),
});

export default connect(null, mapDispatchToProps)(SignUp);
