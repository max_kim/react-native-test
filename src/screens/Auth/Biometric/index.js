import React from 'react';
import { StyleSheet } from 'react-native';
import styled from 'styled-components';

import { palette } from '../../../core/styleGuide';

const Biometric = () => {
  return (
    <Container>
      <View style={StyleSheet.absoluteFill}>
        <Text>{'Biometric Screen'}</Text>
      </View>
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

const View = styled.View``;

const Text = styled.Text`
  font-size: 24px;
  color: ${palette.tundora};
`;

export default Biometric;
