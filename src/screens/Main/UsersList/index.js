import React, { useState, useEffect, useCallback } from 'react';
import { FlatList } from 'react-native';
import styled from 'styled-components';

import Container from '../../../presentations/common/Container';
import { palette } from '../../../core/styleGuide';

import { connect } from 'react-redux';
import { getUsersList } from '../../../store/selectors/users';
import { usersActions } from '../../../store/redux/users/actions';
import { usersActions as asyncActions } from '../../../store/sagas/users/actions';
import screens from '../../../navigation/screens';
import UsersListItem from '../../../presentations/Main/UsersList/UsersListItem';

let nextPage = null;
const setNextPage = (next) => {
  nextPage = next;
};

const UsersList = ({ navigation, usersList, fetchUsers, setCurrentUser }) => {
  const [refreshing, setRefreshing] = useState(false);

  const initialFetchUsers = useCallback(() => {
    fetchUsers({
      page: 1,
      setNextPage: (next) => {
        setNextPage(next);
        setRefreshing(false);
      },
    });
  }, [fetchUsers]);

  const fetchNextPage = useCallback(() => {
    nextPage && fetchUsers({ page: nextPage, setNextPage });
  }, [fetchUsers]);

  useEffect(initialFetchUsers, []);

  const onPressItem = useCallback(
    (id) => {
      setCurrentUser(id);
      navigation.navigate(screens.mainStack.userInfo);
    },
    [navigation, setCurrentUser],
  );

  return (
    <Container>
      <StyledList
        data={usersList}
        renderItem={({ item }) => (
          <UsersListItem item={item} onPress={onPressItem} />
        )}
        keyExtractor={({ id }) => String(id)}
        ItemSeparatorComponent={ItemSeparatorComponent}
        refreshing={refreshing}
        onRefresh={initialFetchUsers}
        onEndReached={fetchNextPage}
        onEndReachedThreshold={3}
      />
    </Container>
  );
};

const StyledList = styled(FlatList).attrs(
  ({ data, contentContainerStyle }) => ({
    contentContainerStyle: {
      flex: Number(!data.length),
      ...contentContainerStyle,
    },
  }),
)`
  width: 100%;
`;

const ItemSeparatorComponent = styled.View`
  border: solid 1px ${palette.mercury};
`;

const mapStateToProps = (state) => ({
  usersList: getUsersList(state),
});

const mapDispatchToProps = (dispatch) => ({
  fetchUsers: (payload) => dispatch(asyncActions.fetchUsers(payload)),
  setCurrentUser: (id) => dispatch(usersActions.setCurrentUser(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
