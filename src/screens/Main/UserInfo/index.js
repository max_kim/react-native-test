import React, { useState, useCallback, useRef } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import Container from '../../../presentations/common/Container';
import Avatar from '../../../presentations/Main/UserInfo/Avatar';
import TextItem from '../../../presentations/common/TextItem';
import Pencil from '../../../presentations/icons/Pencil';
import MediaPicker from '../../../presentations/common/MediaPicker';
import { getCurrentUser } from '../../../store/selectors/users';
import { usersActions } from '../../../store/sagas/users/actions';
import { palette } from '../../../core/styleGuide';
import { errorAlert, hex2rgba } from '../../../core/utils';

const UserInfo = ({ user, updateAvatar }) => {
  const { id, avatar, first_name, last_name, email } = user;

  const mediaPickerRef = useRef(null);

  const [avatarUri, setAvatarUri] = useState(avatar);

  const [isUploading, setIsUploading] = useState(false);

  const onSuccess = useCallback(() => {
    setIsUploading(false);
  }, []);

  const onError = useCallback(
    (message) => {
      errorAlert({ message });
      setAvatarUri(avatar);
      setIsUploading(false);
    },
    [avatar],
  );

  const onPressEditAvatar = useCallback(() => {
    if (isUploading) {
      return;
    }

    setIsUploading(true);

    mediaPickerRef.current?.show();
  }, [isUploading]);

  const onSelectAvatarSuccess = useCallback(
    (response) => {
      setAvatarUri(response?.path);
      updateAvatar(id, response, onSuccess, onError);
    },
    [id, onError, onSuccess, updateAvatar],
  );

  const onSelectAvatarCancel = useCallback(() => {
    setIsUploading(false);
  }, []);

  return (
    <Container>
      <AvatarBlock>
        <Avatar
          uri={avatarUri}
          size={158}
          isUploading={isUploading}
          indicator={'large'}
        />
        <EditAvatarButtonsBlock>
          <EditAvatarButton onPress={onPressEditAvatar}>
            <Pencil />
          </EditAvatarButton>
        </EditAvatarButtonsBlock>
      </AvatarBlock>

      <ItemsWrapper>
        <TextItem title={'first name'} text={first_name} />
        <TextItem title={'last name'} text={last_name} />
        <TextItem title={'email'} text={email} />
      </ItemsWrapper>
      <MediaPicker
        ref={mediaPickerRef}
        title={'Select Avatar'}
        config={{ cropping: true, mediaType: 'photo' }}
        onSuccess={onSelectAvatarSuccess}
        onError={onError}
        onCancel={onSelectAvatarCancel}
      />
    </Container>
  );
};

const AvatarBlock = styled.View`
  align-items: center;
  width: 100%;
  padding-horizontal: 16px;
`;

const EditAvatarButtonsBlock = styled.View`
  position: absolute;
  bottom: 0;
  left: 65%;
  z-index: 1;
  flex-direction: row;
  height: 34px;
  justify-content: center;
  align-items: center;
`;

const EditAvatarButton = styled.TouchableOpacity`
  height: 34px;
  width: 34px;
  border-radius: 17px;
  border-width: 1px;
  border-color: ${hex2rgba(palette.black, 0.3)};
  justify-content: center;
  align-items: center;
  shadow-color: ${palette.black};
  shadow-offset: 0px 0px;
  shadow-opacity: 0.3;
  shadow-radius: 1px;
  background-color: ${palette.alabaster};
`;

const ItemsWrapper = styled.View`
  width: 100%;
  margin-top: 13px;
`;

const mapStateToProps = (state) => ({
  user: getCurrentUser(state),
});

const mapDispatchToProps = (dispatch) => ({
  updateAvatar: (id, avatar, onSuccess, onError) =>
    dispatch(usersActions.updateAvatar({ id, avatar, onSuccess, onError })),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserInfo);
