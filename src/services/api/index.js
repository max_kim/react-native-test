import axios from 'axios';

import { API_ROOT } from '../../core/configs/constants';

const client = axios.create({
  baseURL: API_ROOT,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 5000,
});

const errorInterceptor = (error) => {
  let message = 'Something went wrong.';

  if (error.response) {
    if (error.response.data?.error) {
      message = error.response.data.error;
    }
  } else if (error.request) {
    message = 'Server timeout.';
  } else {
    message = error.message;
  }
  console.log('api error', error.config);

  return Promise.reject({ message });
};

client.interceptors.response.use((response) => response.data, errorInterceptor);

export const setDefaultAuthToken = ({ token }) => {
  client.defaults.headers.common['Authorization'] = token;
};

export default client;
