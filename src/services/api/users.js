import client from './index';

export default {
  getUser: (id) => client.get(`/users/${id}`),
  getUsersList: (page = 1) => client.get(`/users?page=${page}`),
  addNewUser: (data) => client.post('/users', data),
  changeUserData: (id, data) => client.put(`/users/${id}`, data),
  deleteUser: (id) => client.delete(`/users/${id}`),
};
