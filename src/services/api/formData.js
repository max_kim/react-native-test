import { Platform } from 'react-native';

export const createFormData = (key, media, body = {}) => {
  const data = new FormData();

  if (Array.isArray(media)) {
    media.forEach((item) => {
      data.append(key, mediaContent(item));
    });
  } else {
    data.append(key, mediaContent(media));
  }

  Object.keys(body).forEach((key) => {
    data.append(key, body[key]);
  });

  return data;
};

const mediaContent = (media) => ({
  name: media.fileName || media.path?.split('/').pop(),
  type: media.mime,
  uri: Platform.select({
    ios: media.path.replace('file://', ''),
    default: media.path,
  }),
});
