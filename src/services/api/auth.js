import client from './index';

export default {
  current: (id) => client.get(`/users/${id}`),
  login: (email, password) => client.post('/login', { email, password }),
  register: (email, password) => client.post('/register', { email, password }),
};
