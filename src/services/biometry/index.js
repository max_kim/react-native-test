import * as Keychain from 'react-native-keychain';
import TouchID from 'react-native-touch-id';

const isSupported = async () => !!(await TouchID.isSupported());

const isEnabled = async () => {
  const biometryType = await Keychain.getSupportedBiometryType();

  if (!biometryType) {
    throw Error('No credentials');
  }

  return true;
};

const set = async (username, password) => {
  await Keychain.setGenericPassword(username, password, {
    accessControl: Keychain.ACCESS_CONTROL.DEVICE_PASSCODE,
    accessible: Keychain.ACCESSIBLE.ALWAYS,
    securityLevel: Keychain.SECURITY_LEVEL.SECURE_HARDWARE,
  });
};

const reset = async () => {
  await Keychain.resetGenericPassword();
};

const get = async () => {
  const credentials = await Keychain.getGenericPassword({
    authenticationPrompt: config,
    accessControl: Keychain.ACCESS_CONTROL.BIOMETRY_ANY,
  });

  if (typeof credentials === 'object') {
    const { username, password } = credentials;
    const valid = !!(username && password);

    if (valid) {
      return credentials;
    }

    throw Error('Not valid credentials');
  } else {
    throw Error('Unable to retrieve credentials');
  }
};

const config = {
  title: 'Authentication required',
};

export default {
  isSupported,
  isEnabled,
  get,
  set,
  reset,
};
