import React, { useMemo, useCallback } from 'react';
import {
  createStackNavigator,
  HeaderBackButton,
} from '@react-navigation/stack';
import { connect } from 'react-redux';

import UsersListScreen from '../../screens/Main/UsersList';
import UserInfoScreen from '../../screens/Main/UserInfo';
import BiometricScreen from '../../screens/Auth/Biometric';
import HeaderButton from '../../presentations/Navigation/HeaderButton';

import screens from '../screens';
import { withoutHeader } from '../options';
import { authActions } from '../../store/redux/auth/actions';
import { getCurrentUser } from '../../store/selectors/users';

const Stack = createStackNavigator();

const MainStack = ({ navigation, logout, currentUser }) => {
  const headerStyle = useMemo(
      () => ({
        elevation: 0,
      }),
      [],
    ),
    headerTitle = useCallback(() => null, []);

  const usersListHeaderRight = useCallback(
    () => <HeaderButton label={'Logout'} onPress={logout} />,
    [logout],
  );

  return (
    <Stack.Navigator>
      <Stack.Screen
        name={screens.mainStack.usersList}
        component={UsersListScreen}
        options={{
          headerStyle,
          headerTitle,
          headerRight: usersListHeaderRight,
        }}
      />
      <Stack.Screen
        name={screens.mainStack.userInfo}
        component={UserInfoScreen}
        options={{
          headerStyle,
          headerTitle: `${currentUser?.first_name} ${currentUser?.last_name}`,
          headerLeft: () => (
            <HeaderBackButton onPress={() => navigation.goBack()} />
          ),
        }}
      />
      <Stack.Screen
        name={screens.auth.biometric}
        component={BiometricScreen}
        options={{
          ...withoutHeader,
        }}
      />
    </Stack.Navigator>
  );
};

const mapStateToProps = (store) => ({
  currentUser: getCurrentUser(store),
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(authActions.resetToken()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainStack);
