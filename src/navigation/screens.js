export default Object.freeze({
  root: {
    auth: 'Auth',
    mainStack: 'MainStack',
  },
  auth: {
    signIn: 'SignIn',
    signUp: 'SignUp',
    biometric: 'Biometric',
  },
  mainStack: {
    usersList: 'UsersList',
    userInfo: 'UserInfo',
  },
});
