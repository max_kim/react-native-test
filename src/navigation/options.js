import { palette } from '../core/styleGuide';

export const headerStyle = {
  borderBottomWidth: 0,
  elevation: 0,
  backgroundColor: palette.alabaster,
};

export const withHeader = {
  headerShown: true,
  headerMode: 'screen',
};

export const withoutHeader = {
  headerShown: false,
  headerMode: 'none',
};

export const mainTheme = {
  dark: false,
  colors: {
    primary: palette.limedSpruce,
    background: palette.alabaster,
    card: palette.alabaster,
    text: palette.tundora,
    border: palette.mercury,
  },
};
