import React, { useMemo, useCallback } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { connect } from 'react-redux';

import SignInScreen from '../../screens/Auth/SignIn';
import SignUpScreen from '../../screens/Auth/SignUp';
import BiometricScreen from '../../screens/Auth/Biometric';
import HeaderButton from '../../presentations/Navigation/HeaderButton';

import screens from '../screens';
import { withoutHeader } from '../options';
import { getErrorMessage } from '../../store/selectors/auth';
import { authActions } from '../../store/redux/auth/actions';

const Stack = createStackNavigator();

const AuthStack = ({ navigation, errorMessage, resetErrorMessage }) => {
  const navigateTo = useCallback(
    (routePath) => () => {
      errorMessage && resetErrorMessage();
      navigation.navigate(routePath);
    },
    [errorMessage, navigation, resetErrorMessage],
  );

  const onPressSighIn = useCallback(navigateTo(screens.auth.signUp), [
    navigateTo,
  ]);

  const onPressSighUp = useCallback(navigateTo(screens.auth.signIn), [
    navigateTo,
  ]);

  const headerStyle = useMemo(
      () => ({
        elevation: 0,
      }),
      [],
    ),
    headerTitle = useCallback(() => null, []);

  const signInHeaderRight = useCallback(
      () => <HeaderButton label={'Sign up'} onPress={onPressSighIn} />,
      [onPressSighIn],
    ),
    signUpHeaderRight = useCallback(
      () => <HeaderButton label={'Sign in'} onPress={onPressSighUp} />,
      [onPressSighUp],
    );

  return (
    <Stack.Navigator>
      <Stack.Screen
        name={screens.auth.signIn}
        component={SignInScreen}
        options={{
          headerStyle,
          headerTitle,
          headerRight: signInHeaderRight,
        }}
      />
      <Stack.Screen
        name={screens.auth.signUp}
        component={SignUpScreen}
        options={{
          headerStyle,
          headerTitle,
          headerLeft: null,
          headerRight: signUpHeaderRight,
        }}
      />
      <Stack.Screen
        name={screens.auth.biometric}
        component={BiometricScreen}
        options={{
          ...withoutHeader,
        }}
      />
    </Stack.Navigator>
  );
};

const mapStateToProps = (state) => ({
  errorMessage: getErrorMessage(state),
});

const mapDispatchToProps = (dispatch) => ({
  resetErrorMessage: () => dispatch(authActions.resetErrorMessage()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthStack);
