import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { connect } from 'react-redux';

import AuthStack from './Auth';
import MainStack from './Main';

import screens from './screens';
import { withoutHeader } from './options';
import { getToken } from '../store/selectors/auth';

const screensList = {
  [screens.root.auth]: AuthStack,
  [screens.root.mainStack]: MainStack,
};

const Stack = createStackNavigator();

const RootStack = ({ screenName }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        ...withoutHeader,
      }}>
      <Stack.Screen name={screenName} component={screensList[screenName]} />
    </Stack.Navigator>
  );
};

const mapStateToProps = (state) => ({
  screenName: getToken(state) ? screens.root.mainStack : screens.root.auth,
});

export default connect(mapStateToProps)(RootStack);
