import { createSelector } from 'reselect';

const tokenSelector = (state) => state.auth.token;

export const getToken = createSelector([tokenSelector], (state) => state);

const errorMessageSelector = (state) => state.auth.authErrorMessage;

export const getErrorMessage = createSelector(
  [errorMessageSelector],
  (state) => state,
);
