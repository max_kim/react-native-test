import { createSelector } from 'reselect';

const usersListSelector = (store) => store.users.list;

const currentUserSelector = (store) => store.users.currentUser;

export const getUsersList = createSelector(
  [usersListSelector],
  (users) => users,
);

export const getCurrentUser = createSelector(
  [getUsersList, currentUserSelector],
  (users, currentId) => users.find((item) => item.id === currentId),
);

export const getUserById = (state, userId) =>
  createSelector(usersListSelector, (users) =>
    users.find((item) => item.id === userId),
  )(state, userId);
