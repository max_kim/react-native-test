import { all } from 'redux-saga/effects';

import { authSagas } from './auth';
import { usersSagas } from './users';

export default function* root() {
  yield all([...authSagas, ...usersSagas]);
}
