import { takeLatest } from 'redux-saga/effects';

import { authTypes } from './types';
import { login, register } from './workers';

export const authSagas = [
  takeLatest(authTypes.LOGIN, login),
  takeLatest(authTypes.REGISTER, register),
];
