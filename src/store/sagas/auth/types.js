export const authTypes = Object.freeze({
  LOGIN: 'LOGIN',
  REGISTER: 'REGISTER',
});
