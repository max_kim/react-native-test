import { call, put } from 'redux-saga/effects';

import auth from '../../../services/api/auth';
import { setDefaultAuthToken } from '../../../services/api';
import { authActions } from '../../redux/auth/actions';

export function* authWorker({ method, email, password, onSuccess, onError }) {
  try {
    const response = yield call([auth, method], email, password);
    yield put(authActions.setAuthorizedSuccess(response));
    yield call(setDefaultAuthToken, response);

    yield call(onSuccess);
  } catch (e) {
    yield put(authActions.setAuthorizedFail(e.message));
    yield call(onError);
  }
}

export function* login({ payload: { email, password, onSuccess, onError } }) {
  yield call(authWorker, {
    method: 'login',
    email,
    password,
    onSuccess,
    onError,
  });
}

export function* register({
  payload: { email, password, onSuccess, onError },
}) {
  yield call(authWorker, {
    method: 'register',
    email,
    password,
    onSuccess,
    onError,
  });
}
