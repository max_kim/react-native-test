import { authTypes } from './types';

export const authActions = Object.freeze({
  login: (payload) => ({
    type: authTypes.LOGIN,
    payload,
  }),
  register: (payload) => ({
    type: authTypes.REGISTER,
    payload,
  }),
});
