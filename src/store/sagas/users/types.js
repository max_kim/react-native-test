export const usersTypes = Object.freeze({
  FETCH_USERS: 'FETCH_USERS',
  UPDATE_AVATAR: 'UPDATE_AVATAR',
});
