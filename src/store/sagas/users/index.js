import { takeLatest } from 'redux-saga/effects';

import { usersTypes } from './types';
import { fetchUsers, updateAvatar } from './workers';

export const usersSagas = [
  takeLatest(usersTypes.FETCH_USERS, fetchUsers),
  takeLatest(usersTypes.UPDATE_AVATAR, updateAvatar),
];
