import { usersTypes } from './types';

export const usersActions = Object.freeze({
  fetchUsers: (payload) => ({
    type: usersTypes.FETCH_USERS,
    payload,
  }),
  updateAvatar: (payload) => ({
    type: usersTypes.UPDATE_AVATAR,
    payload,
  }),
});
