import { call, put, select } from 'redux-saga/effects';
import _ from 'lodash';

import users from '../../../services/api/users';
import { usersActions } from '../../redux/users/actions';
import { getUsersList } from '../../selectors/users';
import { createFormData } from '../../../services/api/formData';

export function* fetchUsers({ payload: { page, setNextPage } }) {
  try {
    const { data, total_pages } = yield call([users, 'getUsersList'], page);

    const list = yield select(getUsersList);
    const updated = yield call([_, 'unionBy'], list, data, 'id');

    yield put(usersActions.setUsersList(updated));

    yield call(setNextPage, page < total_pages ? ++page : null);
  } catch (e) {}
}

export function* updateUser({ id, data }) {
  const usersList = yield select(getUsersList);

  const updatedList = usersList.map((item) =>
    item.id === id ? { ...item, ...data } : item,
  );

  yield put(usersActions.setUsersList(updatedList));
}

export function* updateAvatar({ payload: { id, avatar, onSuccess, onError } }) {
  try {
    const formData = yield call(createFormData, 'avatar', avatar);
    const data = yield call([users, 'changeUserData'], id, formData);

    yield call(updateUser, id, data);

    yield call(onSuccess);
  } catch (e) {
    yield call(onError, e.message);
  }
}
