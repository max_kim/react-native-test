export const types = Object.freeze({
  SET_AUTHORIZED_SUCCESS: 'SET_AUTHORIZED_SUCCESS',
  SET_AUTHORIZED_FAIL: 'SET_AUTHORIZED_FAIL',
  RESET_TOKEN: 'RESET_TOKEN',
  RESET_ERROR_MESSAGE: 'RESET_ERROR_MESSAGE',
});
