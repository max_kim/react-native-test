import { types } from './types';

export const authActions = Object.freeze({
  setAuthorizedSuccess: (payload) => ({
    type: types.SET_AUTHORIZED_SUCCESS,
    payload,
  }),
  setAuthorizedFail: (payload) => ({
    type: types.SET_AUTHORIZED_FAIL,
    payload,
  }),
  resetToken: (payload) => ({
    type: types.RESET_TOKEN,
    payload,
  }),
  resetErrorMessage: (payload) => ({
    type: types.RESET_ERROR_MESSAGE,
    payload,
  }),
});
