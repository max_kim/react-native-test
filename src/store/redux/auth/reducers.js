import { types } from './types';

const initialState = {
  token: null,
  authErrorMessage: null,
};

export const auth = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_AUTHORIZED_SUCCESS:
      const { token } = action.payload;
      return {
        ...state,
        token,
        authErrorMessage: null,
      };

    case types.SET_AUTHORIZED_FAIL:
      return {
        ...state,
        authErrorMessage: action.payload,
      };

    case types.RESET_TOKEN:
      return {
        ...state,
        token: null,
      };

    case types.RESET_ERROR_MESSAGE:
      return {
        ...state,
        authErrorMessage: null,
      };

    default:
      return state;
  }
};
