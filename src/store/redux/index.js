import { combineReducers } from 'redux';

import { users } from './users/reducer';
import { auth } from './auth/reducers';

export default combineReducers({
  users,
  auth,
});
