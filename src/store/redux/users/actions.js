import { types } from './types';

export const usersActions = Object.freeze({
  setUsersList: (payload) => ({
    type: types.SET_USERS_LIST,
    payload,
  }),
  setCurrentUser: (payload) => ({
    type: types.SET_CURRENT_USER,
    payload,
  }),
});
