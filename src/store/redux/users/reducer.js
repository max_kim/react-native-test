import { types } from './types';

const initialState = {
  list: [],
  currentUser: null,
};

export const users = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_USERS_LIST:
      return {
        ...state,
        list: action.payload,
      };

    case types.SET_CURRENT_USER:
      return {
        ...state,
        currentUser: action.payload,
      };

    default:
      return state;
  }
};
