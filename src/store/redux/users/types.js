export const types = Object.freeze({
  SET_USERS_LIST: 'SET_USERS_LIST',
  SET_CURRENT_USER: 'SET_CURRENT_USER',
});
